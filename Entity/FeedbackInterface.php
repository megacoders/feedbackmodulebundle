<?php

namespace Megacoders\FeedbackModuleBundle\Entity;

interface FeedbackInterface {

    /**
     * @return \DateTime
     */
    public function getDate();

    /**
     * @return string
     */
    public function getName();

    /**
     * @return string
     */
    public function getEmail();

    /**
     * @return string
     */
    public function getMessage();

}
