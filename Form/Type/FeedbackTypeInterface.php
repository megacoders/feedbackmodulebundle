<?php

namespace Megacoders\FeedbackModuleBundle\Form\Type;

use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\FormTypeInterface;

interface FeedbackTypeInterface extends FormTypeInterface {

    /**
     * @param ListMapper $listMapper
     */
    public function configureAdminListFields(ListMapper $listMapper);

    /**
     * @param ShowMapper $showMapper
     */
    public function configureAdminShowFields(ShowMapper $showMapper);

}
