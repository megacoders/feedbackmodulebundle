<?php

namespace Megacoders\FeedbackModuleBundle\Form\Type;

use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

abstract class FeedbackType extends AbstractType implements FeedbackTypeInterface
{
    /**
     * @var string
     */
    protected $notBlankErrorMessage;

    /**
     * CertificateProductType constructor.
     */
    public function __construct()
    {
        $this->notBlankErrorMessage = (new NotBlank())->message;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'admin.entities.feedback.name',
                'required' => true
            ])
            ->add('email', TextType::class, [
                'label' => 'admin.entities.feedback.email',
                'required' => true
            ])
            ->add('message', TextareaType::class, [
                'label' => 'admin.entities.feedback.message',
                'required' => true
            ]);
    }

    /**
     * @param ListMapper $listMapper
     */
    public function configureAdminListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('name', null, ['label' => 'admin.entities.feedback.name'])
            ->add('email', null, ['label' => 'admin.entities.feedback.email']);
    }

    /**
     * @param ShowMapper $showMapper
     */
    public function configureAdminShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('name', null, ['label' => 'admin.entities.feedback.name'])
            ->add('email', null, ['label' => 'admin.entities.feedback.email'])
            ->add('message', null, ['label' => 'admin.entities.feedback.message']);
    }

}
