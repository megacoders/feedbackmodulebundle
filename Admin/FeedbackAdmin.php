<?php

namespace Megacoders\FeedbackModuleBundle\Admin;

use Megacoders\AdminBundle\Admin\BaseAdmin;
use Megacoders\FeedbackModuleBundle\Form\Type\FeedbackTypeInterface;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\FormFactory;

class FeedbackAdmin extends BaseAdmin
{
    /**
     * @var string
     */
    protected $baseRoutePattern = 'messages/feedback';

    /**
     * @var array
     */
    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'DESC',
        '_sort_by' => 'date',
    );

    /**
     * {@inheritdoc}
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->remove('create')
            ->remove('edit');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add(
            'date', null, ['label' => 'admin.entities.feedback.date'],
            'sonata_type_datetime_picker', ['format' => 'yyyy-MM-dd HH:mm']
        );
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->add('date', null, [
            'label' => 'admin.entities.feedback.date',
            'pattern' => 'yyyy-MM-dd HH:mm'
        ]);

        $formType = $this->getFeedbackFormType();
        $formType->configureAdminListFields($listMapper);

        $listMapper->add('_action', null, [
            'label' => 'admin.actions._actions',
            'actions' => ['show' => [], 'delete' => []]
        ]);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $formType = $this->getFeedbackFormType();
        $formType->configureAdminShowFields($showMapper);
    }

    /**
     * @return FeedbackTypeInterface
     */
    private function getFeedbackFormType()
    {
        /** @var ContainerInterface $container */
        $container = $this->getConfigurationPool()->getContainer();

        /** @var FormFactory $formFactory */
        $formFactory = $container->get('form.factory');

        $formTypeClass = $container->getParameter('feedback_form_type_class');
        return new $formTypeClass();
    }

}
