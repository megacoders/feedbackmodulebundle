<?php
namespace Megacoders\FeedbackModuleBundle\Controller;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Megacoders\FeedbackModuleBundle\Entity\FeedbackInterface;
use Megacoders\FeedbackModuleBundle\Form\Type\FeedbackTypeInterface;
use Megacoders\PageBundle\Controller\Module\BaseModuleController;
use Megacoders\PageBundle\Entity\PageBlock;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Translation\TranslatorInterface;

class FeedbackController extends BaseModuleController
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * FeedbackController constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * {@inheritdoc}
     */
    public function configureRoutes($baseName, $baseUrl, PageBlock $block)
    {
        $collection = new RouteCollection();

        /** Feedback form */
        $collection->add($baseName, new Route($baseUrl));

        /** Feedback created */
        $collection->add(
            $baseName .'_done',
            new Route($baseUrl .'/done', ['_action' => 'done'])
        );

        return $collection;
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $form = $this->createFeedbackForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $feedback = $form->getData();
            $this->saveFeedback($feedback);
            $this->sendModeratorNotification($feedback);

            return $this->redirect($this->generateMainBlockUrl('done'));
        }

        return $this->render('index', ['form' => $form->createView()]);
    }

    /**
     * @return Response
     */
    public function doneAction()
    {
        return $this->render('done');
    }

    /**
     * @return FormInterface
     */
    protected function createFeedbackForm()
    {
        $feedbackFormTypeClass = $this->getParameter('feedback_form_type_class');
        $feedbackClass = $this->getParameter('feedback_entity_class');

        /** @var FeedbackTypeInterface $feedback */
        $feedback = new $feedbackClass();

        return $this->createForm($feedbackFormTypeClass, $feedback);
    }

    /**
     * @param FeedbackInterface $feedback
     */
    protected function saveFeedback(FeedbackInterface $feedback)
    {
        $this->entityManager->persist($feedback);
        $this->entityManager->flush($feedback);
    }

    /**
     * @param FeedbackInterface $feedback
     */
    protected function sendModeratorNotification(FeedbackInterface $feedback)
    {
        $moderatorEmail = $this->getModeratorEmail();

        if ($moderatorEmail === null) {
            return;
        }

        /** @var TranslatorInterface $translator */
        $translator = $this->get('translator');

        /** @var \Swift_Mailer $message */
        $mailer = $this->get('mailer');

        $message = new \Swift_Message(
            $translator->trans('modules.feedback.mail.subject'),
            $this->render('notification', ['feedback' => $feedback])->getContent(),
            'text/html'
        );

        $message
            ->setFrom($this->getParameter('mailer_user') ?: $feedback->getEmail())
            ->setTo($moderatorEmail);

        $mailer->send($message);
    }

    /**
     * @return string|null
     */
    protected function getModeratorEmail()
    {
        return $this->getModuleParameter('moderator_email', $this->getParameter('admin_email'));
    }

}
